<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Editace</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/main.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <div class="container px-5">
                <a class="navbar-brand text-light" href="index.php">Konzolisté ČR</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <?php include "navigace.php"; ?>
                    </ul>
                </div>
            </div>
        </nav>

            <!-- Content Row-->
            
      <?php
           if(isset($_SESSION['loggedUser'])) 
           {
               $login = $_SESSION['loggedUser'];
               $admin = $_SESSION['admin'];               

               if($admin==1 && isset($_SESSION['loggedUser']))
               {
                  ?>
                  <div class="row gx-4 gx-lg-5 justify-content-center">
                     <div class="justify-content-center">
                        <h1 class="mt-5 d-flex justify-content-center">Editace obsahu</h1>
                     </div>

                     <?php
                        // Nastavení proměnných pro připojení k databázi
                        $hostName = "localhost";
                        $databaseName = "vojta.vlk";
                        $userName = "root";
                        $password = "";

                        // Připojení k MySQL/MariaDB serveru
                        $idSpojeni = mysqli_connect($hostName,$userName,$password);

                        // Připojení k DB
                        $idDB = mysqli_select_db($idSpojeni, $databaseName);
                        
                        if(isset($_REQUEST['deleteID']))
                           {
                              $deleteID = $_REQUEST['deleteID'];
                              //printf($deleteID);
                              mysqli_query($idSpojeni,"DELETE FROM clanky WHERE id='$deleteID'");
                           }

                           if(isset($_REQUEST['updateID']))
                           {
                              $updateID = $_REQUEST['updateID'];                        
                              
                              $dotazUpdate = mysqli_query($idSpojeni, "SELECT titulek, clanek FROM clanky WHERE id='$updateID'");
                              $pocetUpdate = mysqli_num_rows($dotazUpdate);                        
                              $detailUpdate = mysqli_fetch_array($dotazUpdate);
                           
                           }

                           if(isset($_REQUEST['submit']))
                           {
                              $titulek = $_REQUEST['titulek'];
                              $clanek = $_REQUEST['clanek'];                                             

                              $dotazUpdate = mysqli_query($idSpojeni,"UPDATE clanky SET titulek='$titulek',clanek='$clanek' WHERE id='$updateID'");
                              header('location: editace-form.php');
                           }

                     ?>

               <?php
               if(isset($_REQUEST['updateID']))
               {
               ?>  
                  <hr>
                  <form method="post" class="col-10 mx-5 p-4 justify-content-center">
                        
                        <label>Titulek: </label> <input type="text" name="titulek" value="<?php echo($detailUpdate['titulek']);?>">
                        <div class="mb-3 justify-content-center"> 
                           <label>Článek:</label>
                           <textarea name="clanek" rows="20" class="my-2 col-12">
                              <?php echo($detailUpdate['clanek']);?>
                           </textarea>
                           <br>
                           <input type="submit" value="Update" name="submit" class="btn btn-primary">
                        </div>
                  </form>

                  <?php
                  }
                  ?>


    
    <div class="col-10 mx-2">
    <?php
        
        
        



        $sqlDotaz01 = "SELECT id, titulek, clanek FROM clanky";

        $dotazclanky = mysqli_query($idSpojeni, $sqlDotaz01);

        $pocetZaznamu = mysqli_num_rows($dotazclanky);
       
        ?>
        <hr>
        <table class="table-bordered" border='2' cellspacing='0'>
          <thead>
            <th class="col-1 text-center">ID</th>
            <th class="col-2 text-center">Titulek</th>
            <th class="col-7 text-center">Článek</th>
            <th class="col-1 text-center">Editace</th>
            <th class="col-1 text-center">Vymazání</th>
          </thead>
        

        <?php
        while($detailClanky = mysqli_fetch_row($dotazclanky))
        {
          printf("<tr>");
          for($i=0;$i<3;$i++)
            printf("<td>".$detailClanky[$i]. "</td>");
        ?>
        
        <td><a href="editace-form.php?updateID=<?php echo($detailClanky[0]); ?>">UPDATE</a></td>
        <td><a href="editace-form.php?deleteID=<?php echo($detailClanky[0]); ?>" onclick="return confirm('Skutečně chcete článek s názvem <?php echo($detailClanky[1]); ?> vymazat?');">DELETE</a></td>
        

        <?php
            printf("</tr>");  
        }
        ?>
        
        </table>  
    </div>  
      <?php
               }
               else if($admin==0 && isset($_SESSION['loggedUser']))
               {
               ?>
               <header class="bg-info border-bottom mb-2">
                  <div class="container">
                     <div class="text-center p-3">
                        <?php 
                           printf("<p>Login <b>$login</b>, admin: <b>$admin</b>");                       
                           printf("<p>Role editora</p>");             
                           printf("<p>Nemáš oprávnění upravovat a mazat příspěvky");                                  
                        ?>
                     </div>
                  </div>
               </header>
               <?php
               }
           }
           
           else 
           {
            ?>
            <header class="bg-info border-bottom mb-2">
            <div class="container">
                <div class="text-center p-3">
                    <?php                        
                           printf("<p>Nikdo není přihlášen</p>");
                           printf("<p>Můžeš si jen prohlížet web</p>");                                  
                    ?>
                </div>
            </div>
            </header>
            <?php               
           }                     
      ?>

            </div>
        <!-- Footer-->
        <footer class="py-4 bg-secondary mt-2">
            <div class="container px-4 px-lg-5"><p class="text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
