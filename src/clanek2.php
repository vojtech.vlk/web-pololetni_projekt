<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>článek</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/main.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <div class="container px-5">
            <a class="navbar-brand text-light" href="index.php">Konzolisté ČR</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <?php include "navigace.php"; ?>
                    </ul>
                </div>
            </div>
        </nav>
        
            <!-- Content Row-->
            <div class="row gx-4 gx-lg-5">
                <?php
                    // Nastavení proměnných pro připojení k databázi
                    $hostName = "localhost";
                    $databaseName = "vojta.vlk";
                    $userName = "root";
                    $password = "";

                    // Připojení k MySQL/MariaDB serveru
                    $idSpojeni = mysqli_connect($hostName,$userName,$password);

                    // Připojení k DB
                    $idDB = mysqli_select_db($idSpojeni, $databaseName);
                    
                    // output data of each row
                    ?>
                  <H1>
                  <?php
                     $sqlDotaz01 = "SELECT * FROM clanky ORDER BY id LIMIT 1 OFFSET 1";
                     $result = $idSpojeni->query($sqlDotaz01);

                     if ($result->num_rows > 0) 
                     {
                     // output data of each row
                     while($row = $result->fetch_assoc()) 
                     {
                           echo "" . $row["titulek"]. "<br>";
                     }
                     } 
                     else {
                     echo "";
                     }

                  ?>
                  </H1>
                  <p>
                  <?php
                     $sqlDotaz01 = "SELECT * FROM clanky ORDER BY id LIMIT 1 OFFSET 1";
                     $result = $idSpojeni->query($sqlDotaz01);

                     if ($result->num_rows > 0) 
                     {
                     // output data of each row
                     while($row = $result->fetch_assoc()) 
                     {
                        echo "" . $row["clanek"]. "<br>";
                     }
                     } 
                     else {
                     echo "";
                     }
                    
                        
                     mysqli_close($idSpojeni);

                     ?>
                  </p>
            </div>
        <!-- Footer-->
        <footer class="py-5 bg-secondary fixed-bottom">
            <div class="container px-4 px-lg-5"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
