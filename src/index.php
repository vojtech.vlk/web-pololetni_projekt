<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Blog Home - Start Bootstrap Template</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/main.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg bg-secondary text-dark">
            <div class="container">
                <a class="navbar-brand text-light" href="index.php">Konzolisté ČR</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <?php include "navigace.php"; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Page header with logo and tagline-->
        <header class="bg-info border-bottom mb-2">
            <div class="container">
                <div class="text-center p-3">
                    <?php                        
                        if(isset($_SESSION['loggedUser'])) 
                        {
                            $login = $_SESSION['loggedUser'];
                            $admin = $_SESSION['admin'];
                            printf("<p>Login <b>$login</b>, admin: <b>$admin</b>");

                            if($admin==1 && isset($_SESSION['loggedUser']))
                            {
                                printf("<p>Role administrátora.</p>");             
                                printf("<p>Můžeš vkládat. Můžeš upravovat a mazat všechny příspěvky, tzn. i příspěvky ostatních.");

                            }
                            else if($admin==0 && isset($_SESSION['loggedUser']))
                            {
                                printf("<p>Role editora.</p>");             
                                printf("<p>Můžeš vkládat. Můžeš upravovat a mazat POUZE SVÉ příspěvky.");
                            }
                        }
                        
                        else 
                        {
                            printf("<p>Nikdo není přihlášen</p>");
                            printf("<p>Můžeš si jen prohlížet web</p>");
                        }                                            
                    ?>
                </div>
            </div>
        </header>
        <!-- Page content-->
        <div class="container">                            
            <div class="card mb-4 col-12">
                <a href="#!"><img class="card-img-top" src="images/new_post.png" alt="..." /></a>
                <div class="card-body">
                    <div class="small text-muted"></div>
                    <!-- Napojeni na databazi-->
                    <?php
                        // Nastavení proměnných pro připojení k databázi
                        $hostName = "localhost";
                        $databaseName = "vojta.vlk";
                        $userName = "root";
                        $password = "";

                        // Připojení k MySQL/MariaDB serveru
                        $idSpojeni = mysqli_connect($hostName,$userName,$password);

                        // Připojení k DB
                        $idDB = mysqli_select_db($idSpojeni, $databaseName);

                        $sqlDotaz01 = "SELECT * FROM clanky ORDER BY id";
                        $idDotaz01 = mysqli_query($idSpojeni, $sqlDotaz01);

                        
                    ?>
                    <h2 class="card-title h4">
                        <?php
                        $sqlDotaz01 = "SELECT * FROM clanky ORDER BY id LIMIT 1 OFFSET 0";
                        $result = $idSpojeni->query($sqlDotaz01);

                        if ($result->num_rows > 0) 
                            {
                            // output data of each row
                            while($row = $result->fetch_assoc()) 
                            {
                                echo "" . $row["titulek"]. "<br>";
                            }
                            } 
                            else {
                            echo "";
                            }

                        ?>
                    </h2>
                    <a class="btn btn-primary" href="clanek1.php">Read more →</a>
                </div>
            </div>
            <!-- Blog post-->
            <div class="card mb-4 col-12">
                <a href="#!"><img class="card-img-top" src="images/new_post.png" alt="..." /></a>
                <div class="card-body">
                    <h2 class="card-title h4">
                    <?php
                        $sqlDotaz01 = "SELECT * FROM clanky ORDER BY id LIMIT 1 OFFSET 1";
                        $result = $idSpojeni->query($sqlDotaz01);

                        if ($result->num_rows > 0) 
                            {
                            // output data of each row
                            while($row = $result->fetch_assoc()) 
                            {
                                echo "" . $row["titulek"]. "<br>";
                            }
                            } 
                            else {
                            echo "";
                            }

                    ?>
                    </h2>
                    <a class="btn btn-primary" href="clanek2.php">Read more →</a>
                </div>
            </div>                                                                                
        </div>
        <!-- Odpojení od DB-->
        <?php                             
        mysqli_close($idSpojeni);
        ?>
        <!-- Footer-->
        <footer class="py-5 bg-secondary">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
