<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Potvrzení registrace</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/main.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
            <div class="container px-5">
            <a class="navbar-brand text-light" href="index.php">Konzolisté ČR</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <?php include "navigace.php"; ?>
                    </ul>
                </div>
            </div>
        </nav>
        
            <!-- Content Row-->
            <div class="row gx-4 gx-lg-5">
                <h1>Potvrzení registrace</h1>
                
                <?php
                    // Nastavení proměnných pro připojení k databázi
                    $hostName = "localhost";
                    $databaseName = "vojta.vlk";
                    $userName = "root";
                    $password = "";

                    // Připojení k MySQL/MariaDB serveru
                    $idSpojeni = mysqli_connect($hostName,$userName,$password);

                    // Připojení k DB
                    $idDB = mysqli_select_db($idSpojeni, $databaseName);

                    $login = $_POST["login"];
                    $heslo1 = $_POST["heslo1"];
                    $heslo2 = $_POST["heslo2"];
                    $email = $_POST["email"];
                    $jmeno = $_POST["jmeno"];
                    $prijmeni = $_POST["prijmeni"];
                    $admin = 0;

                    if(!($heslo1==$heslo2))
                        die("<p>Zadaná hesla NEJSOU stejná.</p>");

                        printf("<p>Registrace proběhla úspěšně</p>");
                        printf("<p><a href='index.php'>Přejít na domovskou stránku</a></p>");

                        // Uložení dat z formuláře do tabulky knihy
                        $sqlDotaz = "INSERT INTO users (login, heslo1, heslo2, email, jmeno, prijmeni, admin) VALUES 	('$login',sha1('$heslo1'),'$heslo2','$email','$jmeno','$prijmeni','$admin')";
                        
                        $idDotaz = mysqli_query($idSpojeni, $sqlDotaz); 
                        
                        mysqli_close($idSpojeni);

                ?>

            </div>
        <!-- Footer-->
        <footer class="py-5 bg-secondary fixed-bottom">
            <div class="container px-4 px-lg-5"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
